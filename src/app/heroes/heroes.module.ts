import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroeComponent } from './heroe/heroe.component';
import { ListadoComponent } from './listado/listado.component';




@NgModule({
  declarations: [ // las declaraciones dicen cuales componentes contiene cada modulo
        HeroeComponent,
        ListadoComponent
    ],
    exports: [ // los exports indican cuales componentes seran publicos fuera de este modulo
        ListadoComponent
    ],
    imports: [  //en imports van los modulos
        CommonModule
    ]
})
export class HeroesModule { }
